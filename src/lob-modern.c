/* lob-modern.c

   Copyright (C) 2021 Thien-Thi Nguyen

   This file is part of Guile-PG.

   Guile-PG is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   Guile-PG is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Guile-PG.  If not, see <http://www.gnu.org/licenses/>.  */

static scm_t_port_type *lobp_type;

#define LOBPORTP(x)  (SCM_PORTP (x) && lobp_type == SCM_PORT_TYPE (x))

static SCM
lob_mklobport (SCM conn, Oid oid, int alod, long modes, const char *FUNC_NAME)
{
  SCM port;
  lob_stream *lobp;

  lobp = GCMALLOC (sizeof (lob_stream), lob_name);

  NOINTS ();
  lobp->conn = conn;
  lobp->oid = oid;
  lobp->alod = alod;
  port = scm_c_make_port (lobp_type, modes, (scm_t_bits) lobp);
  INTSOK ();

  return port;
}

static void
lob_flush (SCM port)
{
  /* Do nothing.  */
}

static OFF_T
lob_seek (SCM port, OFF_T offset, int whence)
{
#define FUNC_NAME __func__
  lob_stream *lobp = LOB_STREAM (port);
  PGconn *conn = LOB_CONN (lobp);
  OFF_T ret;

  NOINTS ();
  ret = lo_lseek (conn, lobp->alod, offset, whence);
  INTSOK ();
  if (PROB (ret))
    ERROR ("Error (~S) seeking on lo port ~S", NUM_INT (ret), port);

  return ret;
#undef FUNC_NAME
}

static size_t
lob_fill_input (SCM port, SCM dst, size_t start, size_t count)
{
#define FUNC_NAME __func__
  lob_stream *lobp = LOB_STREAM (port);
  PGconn *conn = LOB_CONN (lobp);
  int ret;

  NOINTS ();
  ret = lo_read (conn, lobp->alod,
                 (char *) SCM_BYTEVECTOR_CONTENTS (dst) + start,
                 count);
  INTSOK ();
  if (PROB (ret))
    ERROR ("Error (~S) reading from lo port ~S", NUM_INT (ret), port);

  return ret;
#undef FUNC_NAME
}

static size_t
lob_write (SCM port, SCM src, size_t start, size_t count)
{
#define FUNC_NAME __func__
  lob_stream *lobp = LOB_STREAM (port);
  PGconn *conn = LOB_CONN (lobp);
  int ret;

  NOINTS ();
  ret = lo_write (conn, lobp->alod,
                  (char *) SCM_BYTEVECTOR_CONTENTS (src) + start,
                  count);
  INTSOK ();
  if (PROB (ret))
    ERROR ("Error (~S) writing to lo port ~S", NUM_INT (ret), port);

  return ret;
#undef FUNC_NAME
}

static void
lob_close (SCM port)
{
  lob_stream *lobp = LOB_STREAM (port);
  PGconn *dbconn = LOB_CONN (lobp);

  NOINTS ();
  /* DWR: Not checking ‘lo_close’ rv.  */
  lo_close (dbconn, lobp->alod);
  INTSOK ();
  lobp->conn = SCM_BOOL_F;
}

static void
init_lob_support (void)
{
  lobp_type = scm_make_port_type ("pg-lo-port", lob_fill_input, lob_write);
  scm_set_port_print         (lobp_type, lob_printpt);
  scm_set_port_close         (lobp_type, lob_close);
  scm_set_port_seek          (lobp_type, lob_seek);
  scm_set_port_needs_close_on_gc (lobp_type, 1);
}

/* lob-modern.c ends here */
